# limit stacksize unlimited
# module load mpich

F90=pgf90
fflags=-O2 -i8 -Mr8 -Mr8intrinsics -ta=tesla,managed,O0 -Minfo=accel,all

all: test

test: Riemann_TwoFluid.o Eigenvec_ISOMHD.o Eos.o Ppos_ISOMHD.o Apps_TwofluidISOMHD.o Eigenvec_IsoEuler.o Ppos_IsoEuler.o Two_Fluid_Source.o

	$(F90) $(fflags) -o x_2fluid_isomhd Riemann_TwoFluid.o Eigenvec_ISOMHD.o Eos.o Ppos_ISOMHD.o Apps_TwofluidISOMHD.o Eigenvec_IsoEuler.o Ppos_IsoEuler.o Two_Fluid_Source.o

Riemann_TwoFluid.o: Riemann_TwoFluid.F directives
	$(F90) $(fflags) Riemann_TwoFluid.F -c

Eigenvec_ISOMHD.o: Eigenvec_ISOMHD.F directives
	$(F90) $(fflags) Eigenvec_ISOMHD.F -c

Eos.o: Eos.F directives
	$(F90) $(fflags) Eos.F -c

Ppos_ISOMHD.o: Ppos_ISOMHD.F directives
	$(F90) $(fflags) Ppos_ISOMHD.F -c

Apps_TwofluidISOMHD.o: Apps_TwofluidISOMHD.F directives
	$(F90) $(fflags) Apps_TwofluidISOMHD.F -c

Eigenvec_IsoEuler.o: Eigenvec_IsoEuler.F directives
	$(F90) $(fflags) Eigenvec_IsoEuler.F -c

Ppos_IsoEuler.o: Ppos_IsoEuler.F directives
	$(F90) $(fflags) Ppos_IsoEuler.F -c

Two_Fluid_Source.o: Two_Fluid_Source.F directives
	$(F90) $(fflags) Two_Fluid_Source.F -c

clean:
	rm *.o  x_2fluid_isomhd -f

